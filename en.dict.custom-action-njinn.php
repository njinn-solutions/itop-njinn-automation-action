<?php
/**
 * Localized data
 *
 * @copyright   Copyright (C) 2021 Njinn Technologies
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

//Class EventNotificationNjinn
Dict::Add('EN US', 'English', 'English', array(
	'Class:EventNotificationNjinn' => 'Njinn Workflow trigger event',
	'Class:EventNotificationNjinn+' => 'Njinn Workflow execution is triggered via webhook',
	'Class:EventNotification/Attribute:webhook_url' => 'Webhook Url'
));

//Class ActionNjinn
Dict::Add('EN US', 'English', 'English', array(
	'Class:ActionNjinn' => 'Njinn Workflow trigger',
	'Class:ActionNjinn+' => 'Triggers Njinn Workflow execution',
	'Class:ActionNjinn/Attribute:webhook_url' => 'Webhook Url'
));
?>