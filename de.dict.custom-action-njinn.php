<?php
/**
 * Localized data
 *
 * @copyright   Copyright (C) 2021 Njinn Technologies
 * @license     http://opensource.org/licenses/AGPL-3.0
 */

//Class EventNotificationNjinn
Dict::Add('DE DE', 'German', 'Deutsch', array(
	'Class:EventNotificationNjinn' => 'Njinn Workflow trigger event',
	'Class:EventNotificationNjinn+' => 'Njinn Workflow via Webhook ausführen',
	'Class:EventNotification/Attribute:webhook_url' => 'Webhook Url'
));

//Class ActionNjinn
Dict::Add('DE DE', 'German', 'Deutsch', array(
	'Class:ActionNjinn' => 'Njinn Workflow ausführen',
	'Class:ActionNjinn+' => 'Njinn Workflow via Webhook ausführen',
	'Class:ActionNjinn/Attribute:webhook_url' => 'Webhook Url'
));
?>