# iTop Njinn Automation Action

Webhook trigger action for the iTop ITSM Portal notification to start Njinn Automation Workflows

Webhook payload works for User Requests only and expects a Custom Form to be associated with the User Requests related iTop Subservice. The payload includes the User Request Id and the user data of the custom form.

## System Requirements
- php-curl extension
- iTop v2.7.3
- iTop Custom Forms v2.0.12

## Open Items
- send Webhook as async task with retries (sync one time only atm)
- add configuration for webhook header attributes
