<?php

require_once(APPROOT.'/core/asynctask.class.inc.php');

class ActionNjinn extends ActionNotification
{
	public static function Init()
	{
		$aParams = array
		(
			"category" => "core/cmdb,application",
			"key_type" => "autoincrement",
			"name_attcode" => "name",
			"state_attcode" => "",
			"reconc_keys" => array('name'),
			"db_table" => "priv_action_njinn",
			"db_key_field" => "id",
			"db_finalclass_field" => "",
			"display_template" => "",
		);
		MetaModel::Init_Params($aParams);
		MetaModel::Init_InheritAttributes();

		MetaModel::Init_AddAttribute(new AttributeString("webhook_url", array("allowed_values"=>null, "sql"=>"webhook_url", "default_value"=>null, "is_null_allowed"=>false, "depends_on"=>array())));

		// Display lists
		MetaModel::Init_SetZListItems('details', array('name', 'description', 'status', 'webhook_url', 'trigger_list'));
		MetaModel::Init_SetZListItems('list', array('name', 'status', 'webhook_url'));
		// Search criteria
		MetaModel::Init_SetZListItems('standard_search', array('name', 'status'));
	}

	public function DoExecute($oTrigger, $aContextArgs)
	{
		if (MetaModel::IsLogEnabledNotification())
		{
			$oLog = new EventNotificationNjinn();
			if ($this->IsBeingTested())
			{
				$oLog->Set('message', 'TEST - Njinn trigger, would call Webhook Url');
			}
			else
			{
				$oLog->Set('message', 'Njinn trigger, calling Webhook Url');
			}
			$oLog->Set('userinfo', UserRights::GetUser());
			$oLog->Set('trigger_id', $oTrigger->GetKey());
			$oLog->Set('action_id', $this->GetKey());
			$oLog->Set('object_id', $aContextArgs['this->object()']->GetKey());
			$oLog->DBInsertNoReload();
		}
		else
		{
			$oLog = null;
		}

		try
		{
			if (!$this->IsBeingTested())
			{
				$sRes = $this->_DoExecute($oTrigger, $aContextArgs, $oLog);
				if ($oLog)
				{	
					$oLog->Set('message', $sRes);
				}
			}
		}
		catch (Exception $e)
		{
			if ($oLog)
			{
				$oLog->Set('message', 'Error: '.$e->getMessage());
			}
		}
		if ($oLog)
		{
			$oLog->DBUpdate();
		}
	}

	protected function _DoExecute($oTrigger, $aContextArgs, &$oLog)
	{
		$sPreviousUrlMaker = ApplicationContext::SetUrlMakerClass();
		try
		{
			$webhook_url   = MetaModel::ApplyParams($this->Get('webhook_url'), $aContextArgs);
		}
		catch(Exception $e)
		{
  			ApplicationContext::SetUrlMakerClass($sPreviousUrlMaker);
  			throw $e;
  		}
		ApplicationContext::SetUrlMakerClass($sPreviousUrlMaker);
		
		if (!is_null($oLog))
		{
			if (isset($webhook_url)) $oLog->Set('webhook_url', $webhook_url);
		}

		try
		{			
			$obj_class = 'UserRequest';
			$obj_key = $aContextArgs['this->object()']->GetKey();
			$oSearch = DBObjectSearch::FromOQL('SELECT TemplateExtraData WHERE obj_class = :obj_class AND obj_key = :obj_key');
			$oSearch->AllowAllData();
			$oSet = new DBObjectSet($oSearch, array(), array('obj_class' => $obj_class, 'obj_key' => $obj_key));
			$oExtraData = $oSet->Fetch();
			$aRawData = unserialize($oExtraData->Get('data'));
			$aData = array('UserRequest' => $obj_key, 'user_data' => $aRawData['user_data']);
			
			$payload = json_encode($aData);

			$ch = curl_init();
			$optArray = array(
					CURLOPT_URL => $webhook_url,
					CURLOPT_HTTPHEADER => array('Content-Type:application/json'),
					CURLOPT_POSTFIELDS => $payload, 
					CURLOPT_RETURNTRANSFER => true
			);
			
			curl_setopt_array($ch, $optArray);
			$result = curl_exec($ch);
			$http_status_code = curl_getinfo($ch, CURLINFO_RESPONSE_CODE); 
			curl_close($ch);

			return 'Webhook sent, Response '.$http_status_code.' '.$result;
		}
		catch (Exception $e)
		{
			return 'Webhook was not sent: '.$e;
		}
	}
}
?>
