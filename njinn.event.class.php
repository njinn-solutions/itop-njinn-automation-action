<?php

class EventNotificationNjinn extends EventNotification
{
	public static function Init()
	{
		$aParams = array
		(
			"category" => "core/cmdb,view_in_gui",
			"key_type" => "autoincrement",
			"name_attcode" => "",
			"state_attcode" => "",
			"reconc_keys" => array(),
			"db_table" => "priv_event_njinn",
			"db_key_field" => "id",
			"db_finalclass_field" => "",
			"display_template" => "",
			"order_by_default" => array('date' => false)
		);
		MetaModel::Init_Params($aParams);
		MetaModel::Init_InheritAttributes();
		MetaModel::Init_AddAttribute(new AttributeString("webhook_url", array("allowed_values"=>null, "sql"=>"webhook_url", "default_value"=>null, "is_null_allowed"=>true, "depends_on"=>array())));
		//MetaModel::Init_AddAttribute(new AttributeString("chat_id", array("allowed_values"=>null, "sql"=>"chat_id", "default_value"=>null, "is_null_allowed"=>true, "depends_on"=>array())));

		// Display lists
		MetaModel::Init_SetZListItems('details', array('date', 'userinfo', 'message', 'trigger_id', 'action_id', 'object_id', 'webhook_url'));
		MetaModel::Init_SetZListItems('list', array('date', 'message', 'webhook_url'));

	}

}
?>