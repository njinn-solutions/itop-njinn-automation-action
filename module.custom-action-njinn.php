<?php
SetupWebPage::AddModule(
	__FILE__,
	'custom-action-njinn/1.0.0',
	array(
		'label' => 'Njinn Automation Notification action',
		'category' => 'business',
		'dependencies' => array(			
		),
		'mandatory' => false,
		'visible' => true,
		'datamodel' => array(
			'njinn.event.class.php',
			'njinn.action.class.php'
		),
		'webservice' => array(
		),
		'data.struct' => array(
		),
		'data.sample' => array(
		),
		'doc.manual_setup' => '',
		'doc.more_information' => '', 
		'settings' => array(
		),
	)
);
?>